# LJindex
Bash script to create a Web index page to ease access to your own, personal copies of Linux Journal magazine in PDF format.

Quick start:
1. Clone the repo
2. cd to repo dir
3. Copy your Linux Journal PDF files to `./LJ/` (with the original file names).
4. Run `./bin/ljindex`
5. Open `index.html` in your browser.
 
The generated index page should work with most browsers when opened as a local file. The folders also contain .htaccess files for use with an Apache web server. These files will allow access to the required html, image and pdf content while blocking the script, templates, etc. Please respect all copyrights on magazine content as described at the foot of the index page.

Download new issues and copy to `./LJ/`
Add descriptions for new issues to the top of `templates/topic-index.txt` in the format
```
<Month> <Year>
<This Months Feature>
```
example:
```
November 2017
Control a Heterogeneous Server Farm with SSH Agent
```
and then re-run the script.


*Linux Journal magazine is copyright © Belltown Media Inc., publishers of Linux Journal. All rights reserved.*

You can subscribe to Linux Journal here 
[Linux Journal](https://www.linuxjournal.com)
 
